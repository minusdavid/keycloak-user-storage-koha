package au.com.prosentient.keycloak.user;

//NOTE: This sets up getters and other useful methods
import lombok.Data;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnore;

//Import for logging...
//import java.util.logging.Logger;
//import java.util.logging.Level;

/**
 * @author Prosentient Systems
 */
@Data
public class KohaUser {

    //NOTE: The @JsonProperty annotation refers to the properties in the JSON response from Koha
    //NOTE: The Java properties get used by the UserAdapter to adapt to the Keycloak UserModel class
    // https://github.com/FasterXML/jackson-databind
    // https://koha/api/v1/.html#op-get-patrons

    @JsonProperty("patron_id")
    private String id;

    //NOTE: In UserAdapter.java, the getUsername() method actually uses email instead of username...
    @JsonProperty("userid")
    private String username;

    @JsonProperty("email")
    private String primary_email;

    @JsonProperty("secondary_email")
    private String secondary_email;

    @JsonProperty("firstname")
    private String firstName;

    @JsonProperty("surname")
    private String lastName;

    @JsonProperty("category_id")
    private String categoryCode;

    //NOTE: This is the real email property that will be used in this extension
    private String email;
    private String password;
    private boolean enabled;
    private Long created;
    private List<String> roles;

    //private static final Logger log = Logger.getLogger(KohaUser.class.getName());

    //NOTE: We used to do this calculation in a @JsonCreator constructor, but it couldn't handle null email arguments
    @JsonIgnore
    public String getEmail() {
        if ( this.email == null ){
            //NOTE: This follows Prosentient legacy logic of preferring emailpro/secondary email over email/primary
            //NOTE: Cache email so future calls are efficient
            if ( this.secondary_email != null && ! this.secondary_email.isEmpty() ){
                this.email = this.secondary_email;
            }
            else if ( this.primary_email != null && ! this.primary_email.isEmpty() ){
                this.email = this.primary_email;
            }
            else {
                this.email = "";
            }
        }
        return this.email;
    }

    //NOTE: No argument constructor allows Jackson to set properties after object constructed
    public KohaUser() {
        this.created = System.currentTimeMillis();
        this.enabled = true;
    }
}
