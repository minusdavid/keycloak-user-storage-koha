package au.com.prosentient.keycloak.user;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.lang.Integer;
import java.util.Optional;

/* Imports for HTTP client */
import java.io.IOException;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpHeaders;
import java.time.Duration;
import java.util.Base64;

//Imports for dealing with JSON
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import org.keycloak.models.UserModel;

//Import for logging...
import java.util.logging.Logger;

/**
 * @author Prosentient Systems
 */
class KohaRepository {

    private List<KohaUser> users;

    private final HttpClient httpClient;

    private final String api_username;
    private final String api_password;
    private final String api_baseurl;
    private String koha_version;

    private static final Logger log = Logger.getLogger(KohaRepository.class.getName());

    private static String basicAuth(String username, String password) {
        return "Basic " + Base64.getEncoder().encodeToString((username + ":" + password).getBytes());
    }

    KohaRepository(String api_username, String api_password, String api_baseurl) {
        this.api_username = api_username;
        this.api_password = api_password;
        this.api_baseurl = api_baseurl;

        this.httpClient = HttpClient.newBuilder()
            .connectTimeout(Duration.ofSeconds(10))
            .version(HttpClient.Version.HTTP_1_1)
            .build();
    }

    //NOTE: These users should be read-only, so this method should be unnecessary...
    public boolean updateCredentials(String username, String password) {
        return false;
    }

    public boolean set_koha_version(String koha_version) {
        this.koha_version = koha_version;
        return true;
    }

    //NOTE: Find individual user...
    //NOTE: BROWSE/DETAIL - when clicking on an individual user DETAIL page in Admin UI BROWSE
    //NOTE: LOGIN - when logging in as a user
    public KohaUser findUserById(String id) {
        return _get_user_by_id(id);
    }

    //NOTE: Find individual user...
    //NOTE: LOGIN - when logging in as a user
    public KohaUser findUserByUsernameOrEmail(String username) {
        return _get_user_by_username_or_email(username);
    }

    //NOTE: This is the method for checking the user's password in the Koha database!
    boolean checkKoha(UserModel user, String password) {
        boolean validity = false;
        String username = user.getUsername();
        KohaUser koha_user = findUserByUsernameOrEmail(username);

        try {
            //log.info("Koha version " + koha_version );
            if ( koha_version == null || koha_version.equals("21.11") ){
                validity = _authenticate_user_custom(koha_user,password);
            }
            else {
                validity = _authenticate_user_core(koha_user,password);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return validity;
    }

    private boolean _authenticate_user_custom(KohaUser koha_user, String password){
        boolean validity = false;
        try {
            String url = String.format("%s/patrons/%s/check_password",api_baseurl,koha_user.getId());
            String payload = String.format("{ \"password\": \"%s\" }",password);

            /* https://stackoverflow.com/questions/54208945/java-11-httpclient-not-sending-basic-authentication */
            HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(payload))
                .header("Authorization", basicAuth(api_username, api_password))
                .header("Content-Type", "application/json")
                .uri(URI.create(url))
                .build();
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            log.info( "Status code " + response.statusCode() );
            if ( response.statusCode() >= 200 && response.statusCode() < 300 ){
                validity = true;
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return validity;
    }

    private boolean _authenticate_user_core(KohaUser koha_user, String password){
        boolean validity = false;
        try {
            String url = String.format("%s/auth/password/validation",api_baseurl);
            //log.info("User " + koha_user.getUsername());
            String payload = String.format("{ \"userid\":\"%s\", \"password\": \"%s\" }",koha_user.getUsername(),password);

            /* https://stackoverflow.com/questions/54208945/java-11-httpclient-not-sending-basic-authentication */
            HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(payload))
                .header("Authorization", basicAuth(api_username, api_password))
                .header("Content-Type", "application/json")
                .uri(URI.create(url))
                .build();
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            log.info( "Status code " + response.statusCode() );
            if ( response.statusCode() >= 200 && response.statusCode() < 300 ){
                validity = true;
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return validity;

    }

    public int get_total_patron_count(String search){
        //log.info( "get_total_patron_count");
        String search_string = _generate_search(search);
        int count = 0;
        String url = String.format("%s/patrons?_per_page=1",this.api_baseurl);
        URI uri = URI.create(url);
        HttpRequest request = HttpRequest.newBuilder()
            .header("Authorization", basicAuth(this.api_username, this.api_password))
            .uri(uri)
            .method("GET",HttpRequest.BodyPublishers.ofString(search_string))
            .build();
        try {
            HttpResponse<String> response = this.httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            log.info( "status code " + response.statusCode() );
            if ( response.statusCode() >= 200 && response.statusCode() < 300 ){
                HttpHeaders headers = response.headers();
                Optional<String> x_total_count_opt = headers.firstValue("X-Total-Count");
                if ( x_total_count_opt.isPresent() ){
                    String x_total_count_str = x_total_count_opt.get();
                    count = Integer.parseInt(x_total_count_str);
                }
            }
        }
        catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        log.info( "get_total_patron_count " + count );
        return count;
    } 

    //NOTE: maxResults works differently from Keycloak 20+
    //From Keycloak 20+, Keycloak asks for 1 more result than it shows, so it knows to paginate
    //Because Koha's API can't handle offset/limit we are going to try to hack together a solution...
    List<KohaUser> getKohaUsers(String search, int firstResult, int maxResults) {
        log.info( "getKohaUsers " + search + " " + firstResult + " " + maxResults );
        int page = 1;
        int per_page = maxResults - 1; // -1 is unlimited in Koha


        int total = get_total_patron_count(search);
        int need_pagination = 1;

        if ( (firstResult + per_page) >= total ){
            need_pagination = 0;
        }

        //NOTE: firstResult is an offset and maxResults is a limit, but
        //this doesn't work with Koha's API. 
        //NOTE: If you use this user provider as the sole user provider in a realm,
        //you can fudge the numbers a bit...
        //NOTE: Since Keycloak 20+, the first page of results needs to retrieve 11 results
        //in order to trigger a 2nd page option even appearing...
        try {
            if ( firstResult == 0 ){
                page = 1;
            }
            //If firstResult > 0, then it's not going to be page 1
            else if ( (firstResult > 0) ){
                int real_per_page = maxResults - 1;
                int dynamic_page = ( firstResult / real_per_page ) + 1;
                page = dynamic_page;
            }
        }
        finally {
        }

        String search_string = _generate_search(search);
        log.info("getKohaUsers search_string " + search_string);

        String url = String.format("%s/patrons?_page=%d&_per_page=%d",api_baseurl,page,per_page);
        log.info( "getKohaUsers url " + url );
        URI uri = URI.create( url );
        HttpRequest request = HttpRequest.newBuilder()
            .header("Authorization", basicAuth(api_username, api_password))
            .uri(uri)
            .method("GET",HttpRequest.BodyPublishers.ofString(search_string))
            .build();

        List<KohaUser> mapped_objects = null;
        try {
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            log.info( "Status code " + response.statusCode() );
            if ( response.statusCode() >= 200 && response.statusCode() < 300 ){
                ObjectMapper mapper = new ObjectMapper()
                    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                mapped_objects = mapper.readValue(response.body(), mapper.getTypeFactory().constructCollectionType(List.class, KohaUser.class));
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        //NOTE: This is a hack.
        //Keycloak 20+ will only paginate if there is 1 more
        //user returned than the requested page size. (maxResults is page size + 1)
        //We do our own calculations and insert an empty
        //user to signify to Keycloak that we need another page.
        if (need_pagination > 0){
            KohaUser empty_user = new KohaUser();
            mapped_objects.add(empty_user);
        }
        return mapped_objects;
    }

    /* Private methods below */

    private String _generate_search(String search){
        String search_string = "";
        if ( ! search.isEmpty() && ! search.equals("*") ){
            //String search_string = String.format("{ \"-or\": [{ \"userid\": {\"-like\": \"%%%s%%\"} }, {\"email\": {\"-like\": \"%%%s%%\"} }, {\"emailpro\": {\"-like\": \"%%%s%%\"} } ] }",search, search, search);
            ObjectMapper writer = new ObjectMapper();
            ArrayNode or_list = writer.createArrayNode();
            String[] koha_fields = {"email","emailpro","userid"};
            ObjectNode condition = writer.createObjectNode();
            condition.put("-like","%" + search + "%");
            for (String field : koha_fields){
                ObjectNode koha_field = writer.createObjectNode();
                koha_field.put(field,condition);
                or_list.add(koha_field);
            }
            ObjectNode payload = writer.createObjectNode();
            payload.set("-or",or_list);
            search_string = payload.toString();
        }
        return search_string;
    }

    private KohaUser _get_user_by_username_or_email(String username){
        KohaUser user = null;

        /* https://stackoverflow.com/questions/54208945/java-11-httpclient-not-sending-basic-authentication */
        String url = String.format("%s/patrons?_per_page=-1",this.api_baseurl);
        URI uri = URI.create( url );

        //String search_string = String.format("{ \"-or\": [{ \"userid\": \"%s\" }, {\"email\": \"%s\"}, {\"emailpro\": \"%s\"} ] }",username,username,username);
        ObjectMapper writer = new ObjectMapper();
        ArrayNode or_list = writer.createArrayNode();
        String[] koha_fields = {"email","emailpro","userid"};
        for (String field : koha_fields){
            ObjectNode koha_field = writer.createObjectNode();
            koha_field.put(field,username);
            or_list.add(koha_field);
        }
        ObjectNode payload = writer.createObjectNode();
        payload.set("-or",or_list);
        String search_string = payload.toString();

        HttpRequest request = HttpRequest.newBuilder()
            .header("Authorization", basicAuth(this.api_username, this.api_password))
            .uri(uri)
            .method("GET",HttpRequest.BodyPublishers.ofString(search_string))
            .build();

        List<KohaUser> mapped_objects = null;
        try {
            HttpResponse<String> response = this.httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            log.info( "Status code " + response.statusCode() );
            if ( response.statusCode() >= 200 && response.statusCode() < 300 ){
                ObjectMapper mapper = new ObjectMapper()
                    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                mapped_objects = mapper.readValue(response.body(), mapper.getTypeFactory().constructCollectionType(List.class, KohaUser.class));
                if ( mapped_objects.size() == 1 ){
                    user = mapped_objects.get(0);
                }
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return user;
    }

    private KohaUser _get_user_by_id(String id){
        KohaUser user = null;

        /* https://stackoverflow.com/questions/54208945/java-11-httpclient-not-sending-basic-authentication */
        String url = String.format("%s/patrons?_per_page=-1",this.api_baseurl);
        URI uri = URI.create( url );

        //String search_string = String.format("{  \"borrowernumber\": \"%s\" }",id);
        ObjectMapper writer = new ObjectMapper();
        ObjectNode node = writer.createObjectNode();
        node.put("borrowernumber",id);
        String search_string = node.toString();

        HttpRequest request = HttpRequest.newBuilder()
            .header("Authorization", basicAuth(this.api_username, this.api_password))
            .uri(uri)
            .method("GET",HttpRequest.BodyPublishers.ofString(search_string))
            .build();

        List<KohaUser> mapped_objects = null;
        try {
            HttpResponse<String> response = this.httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            log.info( "Status code " + response.statusCode() );
            if ( response.statusCode() >= 200 && response.statusCode() < 300 ){
                ObjectMapper mapper = new ObjectMapper()
                    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                mapped_objects = mapper.readValue(response.body(), mapper.getTypeFactory().constructCollectionType(List.class, KohaUser.class));
                if ( mapped_objects.size() == 1 ){
                    user = mapped_objects.get(0);
                }
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return user;
    }
}
