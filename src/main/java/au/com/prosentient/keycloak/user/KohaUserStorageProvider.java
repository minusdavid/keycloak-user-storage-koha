package au.com.prosentient.keycloak.user;

import org.keycloak.component.ComponentModel;
import org.keycloak.credential.CredentialInput;
import org.keycloak.credential.CredentialInputUpdater;
import org.keycloak.credential.CredentialInputValidator;
import org.keycloak.models.GroupModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserCredentialModel;
import org.keycloak.models.UserModel;
import org.keycloak.models.credential.PasswordCredentialModel;
import org.keycloak.storage.StorageId;
import org.keycloak.storage.UserStorageProvider;
import org.keycloak.storage.user.UserLookupProvider;
import org.keycloak.storage.user.UserQueryProvider;
import org.keycloak.storage.user.UserRegistrationProvider;

import java.util.Map;
import java.util.stream.Stream;

//Import for logging...
import java.util.logging.Logger;

/**
 * @author Prosentient Systems
 */
public class KohaUserStorageProvider implements UserStorageProvider,
        UserLookupProvider, UserQueryProvider, CredentialInputUpdater, CredentialInputValidator,
        UserRegistrationProvider {

    private final KeycloakSession session;
    private final ComponentModel model;
    private final KohaRepository repository;

    //Add logger
    private static final Logger log = Logger.getLogger(KohaUserStorageProvider.class.getName());

    public KohaUserStorageProvider(KeycloakSession session, ComponentModel model, KohaRepository repository) {
        this.session = session;
        this.model = model;
        this.repository = repository;
    }

    @Override
    public boolean supportsCredentialType(String credentialType) {
        return PasswordCredentialModel.TYPE.equals(credentialType);
    }

    @Override
    public boolean isConfiguredFor(RealmModel realm, UserModel user, String credentialType) {
        return supportsCredentialType(credentialType);
    }

    @Override
    public boolean isValid(RealmModel realm, UserModel user, CredentialInput input) {
        if (!supportsCredentialType(input.getType()) || !(input instanceof UserCredentialModel)) {
            return false;
        }
        UserCredentialModel cred = (UserCredentialModel) input;
        return repository.checkKoha(user, cred.getChallengeResponse());
    }

    @Override
    public boolean updateCredential(RealmModel realm, UserModel user, CredentialInput input) {
        if (!supportsCredentialType(input.getType()) || !(input instanceof UserCredentialModel)) {
            return false;
        }
        UserCredentialModel cred = (UserCredentialModel) input;
        return repository.updateCredentials(user.getUsername(), cred.getChallengeResponse());
    }

    @Override
    public void disableCredentialType(RealmModel realm, UserModel user, String credentialType) {
    }

    @Override
    public Stream<String> getDisableableCredentialTypesStream(RealmModel realm, UserModel user) {
        return Stream.empty();
    }

    @Override
    public void close() {
    }

    //https://www.keycloak.org/docs-api/17.0/javadocs/org/keycloak/storage/user/UserLookupProvider.html
    //https://www.keycloak.org/docs-api/17.0/javadocs/org/keycloak/storage/user/UserQueryProvider.html
    //https://www.keycloak.org/docs-api/17.0/javadocs/org/keycloak/storage/user/UserRegistrationProvider.html

    //NOTE: UserLookupProvider interface (required for user login)
    //NOTE: Used by user "detail" page
    //NOTE: Used by user login (after getUserByEmail/getUserByUsername)
    @Override
    public UserModel getUserById(RealmModel realm, String id) {
        log.info("getUserById - realm id");
        String externalId = StorageId.externalId(id);
        return new UserAdapter(session, realm, model, repository.findUserById(externalId));
    }

    //NOTE: UserLookupProvider interface (required for user login)
    //NOTE: Used by user login by username
    @Override
    public UserModel getUserByUsername(RealmModel realm, String username) {
        log.info("getUserByUsername - realm username");
        KohaUser user = repository.findUserByUsernameOrEmail(username);
        if (user != null) {
            return new UserAdapter(session, realm, model, user);
        }
        return null;
    }

    //NOTE: UserLookupProvider interface (required for user login)
    //NOTE: Used by user login by email
    @Override
    public UserModel getUserByEmail(RealmModel realm, String email) {
        log.info("getUserByEmail - realm email");
        return getUserByUsername(realm, email);
    }

    //NOTE: UserQueryProvider interface (required for viewing and managing users from admin console)
    //NOTE: Used by SEARCH in "Users" area
    @Override
    public int getUsersCount(RealmModel realm, String search){
        log.info("getUsersCount - realm search " + search);
        return repository.get_total_patron_count(search);
    }

    @Override
    public Stream<UserModel> searchForUserStream(RealmModel realm, String search, Integer firstResult, Integer maxResults) {
        log.info("searchForUser - realm, search, firstResult, maxResults");
        return repository.getKohaUsers(search,firstResult,maxResults).stream()
                .map(user -> new UserAdapter(session, realm, model, user));
    }

    //NOTE: UserQueryProvider interface (required for viewing and managing users from admin console)
    //NOTE: For older Keycloak versions, used by BROWSE in "Users" area (when clicking "View all users" button)
    //NOTE: In Keycloak 20+, it doesn't appear to be used... since the "View all users" button no longer exists.
        //NOTE: "params" appears to be empty while browsing...
    @Override
    public int getUsersCount(RealmModel realm, Map<String, String> params){
        log.info("getUsersCount - realm params");
        return repository.get_total_patron_count("");
    }
    @Override
    public Stream<UserModel> searchForUserStream(RealmModel realm, Map<String, String> params, Integer firstResult, Integer maxResults) {
        log.info("searchForUser - realm, params, firstResult, maxResults " + params.toString());
        return repository.getKohaUsers("",firstResult,maxResults).stream()
                .map(user -> new UserAdapter(session, realm, model, user));
    }

    /* 
        Below methods appear to be unused but needed for compiling... 
    */

    //NOTE: UserQueryProvider interface (required for viewing and managing users from admin console)
    @Override
    public int getUsersCount(RealmModel realm) {
        log.info("getUsersCount - realm");
        return 0;
    }

    //NOTE: UserQueryProvider interface (required for viewing and managing users from admin console)
    @Override
    public Stream<UserModel> getUsersStream(RealmModel realm) {
        return Stream.empty();
    }

    //NOTE: UserQueryProvider interface (required for viewing and managing users from admin console)
    //NOTE: Appears to be unused but needed for compiling
    @Override
    public Stream<UserModel> getUsersStream(RealmModel realm, Integer firstResult, Integer maxResults) {
        return Stream.empty();
    }

    //NOTE: UserQueryProvider interface (required for viewing and managing users from admin console)
    @Override
    public Stream<UserModel> searchForUserStream(RealmModel realm, String search) {
        return Stream.empty();
    }

    //NOTE: UserQueryProvider interface (required for viewing and managing users from admin console)
    @Override
    public Stream<UserModel> searchForUserStream(RealmModel realm, Map<String, String> params) {
        return Stream.empty();
    }

    //NOTE: UserQueryProvider interface (required for viewing and managing users from admin console)
    @Override
    public Stream<UserModel> getGroupMembersStream(RealmModel realm, GroupModel group, Integer firstResult, Integer maxResults) {
        return Stream.empty();
    }

    //NOTE: UserQueryProvider interface (required for viewing and managing users from admin console)
    @Override
    public Stream<UserModel> getGroupMembersStream(RealmModel realm, GroupModel group) {
        return Stream.empty();
    }

    //NOTE: UserQueryProvider interface (required for viewing and managing users from admin console)
    @Override
    public Stream<UserModel> searchForUserByUserAttributeStream(RealmModel realm, String attrName, String attrValue) {
        return Stream.empty();
    }

    //NOTE: UserRegistrationProvider (required for adding users into Koha backend via Keycloak)
    @Override
    public UserModel addUser(RealmModel realm, String username) {
        return null;
    }

    //NOTE: UserRegistrationProvider (required for removing users from Koha backend via Keycloak)
    @Override
    public boolean removeUser(RealmModel realm, UserModel user) {
        return false;
    }
}
