package au.com.prosentient.keycloak.user;

import org.keycloak.component.ComponentModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.provider.ProviderConfigurationBuilder;
import org.keycloak.storage.UserStorageProviderFactory;

import java.util.List;

/**
 * @author Prosentient Systems
 */
public class KohaUserStorageProviderFactory implements UserStorageProviderFactory<KohaUserStorageProvider> {

    @Override
    public KohaUserStorageProvider create(KeycloakSession session, ComponentModel model) {
        // here you can setup the user storage provider, initiate some connections, etc.

        String api_username = model.getConfig().getFirst("api_username");
        String api_password = model.getConfig().getFirst("api_password");
        String api_baseurl = model.getConfig().getFirst("api_baseurl");
        String koha_version = model.getConfig().getFirst("koha_version");

        KohaRepository repository = new KohaRepository(api_username, api_password, api_baseurl);
        repository.set_koha_version(koha_version);

        return new KohaUserStorageProvider(session, model, repository);
    }

    @Override
    public String getId() {
        return "koha-user-provider";
    }

    @Override
    public List<ProviderConfigProperty> getConfigProperties() {
        return ProviderConfigurationBuilder.create()
                .property("api_baseurl", "Koha API URL", "URL to use for Koha API", ProviderConfigProperty.STRING_TYPE, "http://koha:8081/api/v1", null)
                .property("api_username", "Koha API Username", "Username to use for Koha API", ProviderConfigProperty.STRING_TYPE, "koha", null)
                .property("api_password", "Koha API Password", "Password to use for Koha API", ProviderConfigProperty.PASSWORD, "koha", null)
                .property("koha_version", "Koha Version", "Koha version", ProviderConfigProperty.STRING_TYPE, "23.05", null)
                .build();
    }
}
