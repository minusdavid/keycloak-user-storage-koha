package au.com.prosentient.keycloak.user;

import org.keycloak.common.util.MultivaluedHashMap;
import org.keycloak.component.ComponentModel;
import org.keycloak.credential.LegacyUserCredentialManager;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.RoleModel;
import org.keycloak.models.SubjectCredentialManager;
import org.keycloak.models.UserModel;
import org.keycloak.storage.StorageId;
import org.keycloak.storage.adapter.AbstractUserAdapter;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

//Import for logging...
import java.util.logging.Logger;

/**
 * @author Prosentient Systems
 */
public class UserAdapter extends AbstractUserAdapter {

    private final KohaUser user;

    private static final Logger log = Logger.getLogger(UserAdapter.class.getName());

    public UserAdapter(KeycloakSession session, RealmModel realm, ComponentModel model, KohaUser user) {
        super(session, realm, model);
        this.storageId = new StorageId(storageProviderModel.getId(), user.getId());
        this.user = user;
    }

    @Override
    public String getUsername() {
        //return user.getEmail();
        return user.getUsername();
    }

    @Override
    public String getFirstName() {
        return user.getFirstName();
    }

    @Override
    public String getLastName() {
        return user.getLastName();
    }

    @Override
    public String getEmail() {
        return user.getEmail();
    }

    @Override
    public SubjectCredentialManager credentialManager() {
        return new LegacyUserCredentialManager(session, realm, this);
    }

    @Override
    public boolean isEnabled() {
        return user.isEnabled();
    }

    @Override
    public Long getCreatedTimestamp() {
        return user.getCreated();
    }

    @Override
    public Map<String, List<String>> getAttributes() {
        MultivaluedHashMap<String, String> attributes = new MultivaluedHashMap<>();
        attributes.add(UserModel.USERNAME, getUsername());
        attributes.add(UserModel.EMAIL, getEmail());
        attributes.add(UserModel.FIRST_NAME, getFirstName());
        attributes.add(UserModel.LAST_NAME, getLastName());
        //Koha-specific attributes
        attributes.add("borrowernumber", user.getId());
        attributes.add("categorycode", user.getCategoryCode());
        return attributes;
    }

    @Override
    public Stream<String> getAttributeStream(String name) {
        try {
            if (name.equals(UserModel.USERNAME)) {
                return Stream.of(getUsername());
            }
            else {
                Map<String, List<String>> attributes = getAttributes();
                //log.info( "Attributes " + attributes.toString() );
                if ( attributes != null && ! attributes.isEmpty() ){
                    //log.info( "Name " + name );
                    List<String> attribute = attributes.get(name);
                    if ( attribute != null && ! attribute.isEmpty() ){
                        //log.info( "Attribute " + attribute.toString() );
                        return attribute.stream();
                    }
                    else {
                        return Stream.empty();
                    }
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return Stream.empty();
    }

    @Override
    protected Set<RoleModel> getRoleMappingsInternal() {
        if (user.getRoles() != null) {
            return user.getRoles().stream().map(roleName -> new UserRoleModel(roleName, realm)).collect(Collectors.toSet());
        }
        return Set.of();
    }
}
