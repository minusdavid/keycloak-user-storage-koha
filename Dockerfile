FROM quay.io/keycloak/keycloak:latest as builder

ADD --chown=keycloak:keycloak target/au.com.prosentient.keycloak-user-storage-koha.jar /opt/keycloak/providers/au.com.prosentient.keycloak-user-storage-koha.jar

RUN /opt/keycloak/bin/kc.sh build

FROM quay.io/keycloak/keycloak:latest

COPY --from=builder /opt/keycloak/ /opt/keycloak/


